#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <cstdlib>
#include "CImg.h"

#define ROUND( a ) ( ( (a) < 0 ) ? (int) ( (a) - 0.5 ) : (int) ( (a) + 0.5 ) )

using namespace std;
using namespace cimg_library;
/**
* JPEG ENCODER
*
*/
float DCT(int i, int j, const CImg<float>& sub_image){
  const unsigned int N = 8; // unsigned = positif
  const float Ci = (i==0)?1.0/sqrt(2):1.0;
  const float Cj = (j==0)?1.0/sqrt(2):1.0;
  float Yij=0.0;

  for (int x=0 ; x<=N-1; ++x)
  {
    for (unsigned int y=0 ; y<=N-1; ++y)
    {
      const float Ixy = float(sub_image(x,y))-128;
      Yij += (Ixy * cos((2.*x+1) * float(i) * M_PI / (2.*float(N))) * cos((2.*y+1.) * float(j) * M_PI / (2.*float(N))));
    }
  }
  return (2.0 / float(N)) * Ci * Cj * Yij;
}


CImg<float> JPEGEncoder(const CImg<float>& image, float quality)
{
 CImg<float> compressed(image.width(),image.height(),1,1,0);
 CImg<float> sub_image(8,8,1,1,0);

 // Quantization matrix
 CImg<float> Q(8,8);
 Q(0,0)=16;   Q(0,1)=11;   Q(0,2)=10;   Q(0,3)=16;   Q(0,4)=24;   Q(0,5)=40;   Q(0,6)=51;   Q(0,7)=61;
 Q(1,0)=12;   Q(1,1)=12;   Q(1,2)=14;   Q(1,3)=19;   Q(1,4)=26;   Q(1,5)=58;   Q(1,6)=60;   Q(1,7)=55;
 Q(2,0)=14;   Q(2,1)=13;   Q(2,2)=16;   Q(2,3)=24;   Q(2,4)=40;   Q(2,5)=57;   Q(2,6)=69;   Q(2,7)=56;
 Q(3,0)=14;   Q(3,1)=17;   Q(3,2)=22;   Q(3,3)=29;   Q(3,4)=51;   Q(3,5)=87;   Q(3,6)=80;   Q(3,7)=62;
 Q(4,0)=18;   Q(4,1)=22;   Q(4,2)=37;   Q(4,3)=56;   Q(4,4)=68;   Q(4,5)=109;  Q(4,6)=103;  Q(4,7)=77;
 Q(5,0)=24;   Q(5,1)=35;   Q(5,2)=55;   Q(5,3)=64;   Q(5,4)=81;   Q(5,5)=104;  Q(5,6)=113;  Q(5,7)=92;
 Q(6,0)=49;   Q(6,1)=64;   Q(6,2)=78;   Q(6,3)=87;   Q(6,4)=103;  Q(6,5)=121;  Q(6,6)=120;  Q(6,7)=101;
 Q(7,0)=72;   Q(7,1)=92;   Q(7,2)=95;   Q(7,3)=98;   Q(7,4)=112;  Q(7,5)=100;  Q(7,6)=103;  Q(7,7)=99;
 Q *= quality;

 CImg<float> patch(8,8);

 for (int row=0 ; row<image.width() ; row+=8) //for each row
 {
   for(int col=0 ; col<image.height() ; col+=8) //for each col
   {
     sub_image = image.get_crop(row,col,row+7,col+7);

     //in patch of 8x8
     for (int i=0 ; i<=7 ; ++i) //row iterator
     {
       for (int j=0 ; j<=7 ; ++j) //col iterator
       {
         //compute DCT
         patch(i,j) = DCT(i,j,sub_image);

         //quantification
         const float quantified = ROUND(float(patch(i,j))/float(Q(i,j)));
         compressed(row+i, col+j) = quantified + 128;
       }
     }
   }
 }

 return compressed;
}












/**
* JPEG DECODER
*
*/

float DCTinv(int x, int y, const CImg<float>& sub_image){
  const unsigned int N = 8; // unsigned = positif
  float Ixy=0.0;

   // Quantization matrix
   CImg<float> Q(8,8);
   Q(0,0)=16;   Q(0,1)=11;   Q(0,2)=10;   Q(0,3)=16;   Q(0,4)=24;   Q(0,5)=40;   Q(0,6)=51;   Q(0,7)=61;
   Q(1,0)=12;   Q(1,1)=12;   Q(1,2)=14;   Q(1,3)=19;   Q(1,4)=26;   Q(1,5)=58;   Q(1,6)=60;   Q(1,7)=55;
   Q(2,0)=14;   Q(2,1)=13;   Q(2,2)=16;   Q(2,3)=24;   Q(2,4)=40;   Q(2,5)=57;   Q(2,6)=69;   Q(2,7)=56;
   Q(3,0)=14;   Q(3,1)=17;   Q(3,2)=22;   Q(3,3)=29;   Q(3,4)=51;   Q(3,5)=87;   Q(3,6)=80;   Q(3,7)=62;
   Q(4,0)=18;   Q(4,1)=22;   Q(4,2)=37;   Q(4,3)=56;   Q(4,4)=68;   Q(4,5)=109;  Q(4,6)=103;  Q(4,7)=77;
   Q(5,0)=24;   Q(5,1)=35;   Q(5,2)=55;   Q(5,3)=64;   Q(5,4)=81;   Q(5,5)=104;  Q(5,6)=113;  Q(5,7)=92;
   Q(6,0)=49;   Q(6,1)=64;   Q(6,2)=78;   Q(6,3)=87;   Q(6,4)=103;  Q(6,5)=121;  Q(6,6)=120;  Q(6,7)=101;
   Q(7,0)=72;   Q(7,1)=92;   Q(7,2)=95;   Q(7,3)=98;   Q(7,4)=112;  Q(7,5)=100;  Q(7,6)=103;  Q(7,7)=99;
   //Q *= quality;


  for (unsigned int i=0 ; i<=(N-1) ; ++i)
  {
    for (unsigned int j=0 ; j<=(N-1) ; ++j)
    {
      const float Yqij = sub_image(i,j) - 128;
      const float Yij = Yqij * Q(i,j); //dequantification
      const float Ci = (i==0)?1.0/sqrt(2):1.0;
      const float Cj = (j==0)?1.0/sqrt(2):1.0;
      Ixy += Ci * Cj * Yij * cos((2*x+1) * i * M_PI / (2.0*float(N))) * cos((2*y+1) * j * M_PI / (2.0*float(N)));
    }
  }

  return (2.0 / float(N)) * Ixy; // deshift
}


CImg<float> JPEGDecoder(const CImg<float>& image, float quality)
{
  CImg<float> decompressed(image.width(),image.height(),1,1,0);
  CImg<float> sub_image(8,8,1,1,0);

  // TODO: code to insert
  CImg<float> patch(8,8);

  for (int row=0 ; row<image.width() ; row+=8) //for each row
  {
    for(int col=0 ; col<image.height() ; col+=8) //for each col
    {
      sub_image = image.get_crop(row,col,row+7,col+7);

      //in patch of 8x8
      for (int x=0 ; x<=7 ; ++x) //row iterator
      {
        for (int y=0 ; y<=7 ; ++y) //col iterator
        {
          //compute DCT
          patch(x,y) = DCTinv(x,y,sub_image);

          decompressed(row+x, col+y) = patch(x,y)+128;
        }
      }
    }
  }

  return decompressed;
}







float tauxDistorsion(CImg<float> original, CImg<float> comparaison){
  float sumTotal = 0.;
  for(int col=0;col<original.width();col++){
    for(int line=0;line<original.height();line++){
      sumTotal += abs((original(col, line) * original(col, line))-(comparaison(col, line) * comparaison(col, line)));
    }
  }
  return  sumTotal / float(original.width()*original.height());
}






int main()
{
 // Read the image "lena.bmp"
 CImg<float> my_image("./lena.bmp");

 // Take the luminance information
 my_image.channel(0);

for (float quality=1.;quality<10;quality++){
 cout << ".\r";cout.flush();
 CImg<float> comp_image = JPEGEncoder(my_image,quality);
  cout << "..\r";cout.flush();
 CImg<float> decomp_image = JPEGDecoder(comp_image,quality);


  cout << "Q=" << quality << " // D="<< tauxDistorsion(my_image, decomp_image)<<"\n";
}


 cout << ".\r";cout.flush();
 CImg<float> comp_image = JPEGEncoder(my_image,1.);
  cout << "..\r";cout.flush();
 CImg<float> decomp_image = JPEGDecoder(comp_image,1.);
 // Display the bmp file
 CImgDisplay main_disp(my_image,"Initial Image");

 // Display the compressed file (by dct)
 CImgDisplay comp_disp(comp_image,"Compressed Image");

 //CImgDisplay decomp_disp(decomp_image,"Decompressed Image");


 while (!main_disp.is_closed())
 {
  main_disp.wait();
 }
}
